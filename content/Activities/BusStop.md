# Bus Stop
--- meta
title: Bus Stop
uuid: 7b6d19c8-1b71-4bf6-8425-808a1059dda3
lan: en
source: Tactical Tech
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Collecting ideas
duration: 15
description: Brainstorm around connected topics or questions.
---

## Meta information

### Description
Brainstorm around connected topics or questions.


### Duration
15 minutes.


### Ideal Number of Participants
Minimum of 6.


### Learning Objectives
##### Knowledge
- Get a wider view on a specific topic by learning what the others in the group think about it.

### Materials and Equipment Needed
--- meta
uuid: e74a1559-b745-4a63-88e6-e3028b07e12c
tag: materials activity
---

- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
- @[material](c0358b51-fe16-47ae-9686-927ec39d18f6)


## Activity: "Bus Stop"
--- meta
uuid: 056617f0-88f0-4bd6-aa25-50b1150b48db
---

#### Preparation
1. Prepare at least 3 connected topics or questions for the group to discuss.
2. Write each question or topic at the top of a large sheet of paper, and hang these up on the wall in different parts of the room. Each paper represents one "bus stop". There should be a minimum of 3 stops - though the number will depend on the number of participants.


#### Bus Stop
1. If there are 3 bus stops, split participants into 3 groups. If the number of participants is very large, make more bus stops. Ask each group to start at a different bus stop.  
2. Each group has 2-3 minutes at their bus stop to write down as many words, terms, phrases and drawings they can think of that relate to the topic/question written at the top of the paper.
3. After 2-3 minutes the bus comes, and each group has to rotate one bus stop to the right. They then go through the same process again, adding to what's already there.
4. Continue rotating until all groups have been to all bus stops.


-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
