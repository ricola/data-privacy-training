# Visualising Tracking in the Browser
--- meta
title: Visualising Tracking in the Browser
uuid: b6953dff-a5bc-4f97-af37-57f05a35817b
lan: en
source: Tactical Tech
item: Activity
tags:
  - Mobile
  - Browser
duration: 30
description: See how tracking in the browser works.
dependencies: "@[snippet](e0d34b63-d015-4338-9081-7ac67f26da92)"
---

## Meta information

### Description
See how tracking in the browser works.


### Duration
30 minutes.


### Ideal Number of Participants
Minumum of 4 participants.


### Learning Objectives
##### Knowledge
- See how tracking in the browser works, and learn about trace-routes: how data travels across the internet.
- Understand what types of data are being collected, and by who; what is meant by profiling; how companies can track individuals across websites; and what browser fingerprinting is.

### References
- Download the _Browser Reference Document_ from the link below, or find it [here](https://myshadow.org/materials)
- [Trackography](https://trackography.org) (Tactical Tech)
- [Lightbeam](https://myshadow.org/resources/lightbeam?locale=en)
- [Panopticlick](https://myshadow.org/resources/panopticlick?locale=en), (EFF)


### Materials and Equipment Needed
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
- @[material](1544909d-e400-4588-a4cb-b638a204422e)

### Optional Materials and Handouts
- @[material](ce39f7f2-0357-419a-aca7-81806e00e6cf)



## Activity: "Visualising tracking in the browser"
--- meta
uuid: ea61792c-218f-48a0-8a9c-47fa611f07d3
---

#### Preparation
1. Make sure you are familiar with Trackography and Lightbeam. More info can be found in the _Reference Document for Browsers_.
2. Make sure you have enough computers available - one per small group. Lightbeam can't be installed on a mobile phone, and Trackography only works on larger screen.
3. On the board, write up the links for Trackography and Lightbeam, or project these onto a screen.
4. If there is limited internet, download the Trackography movie and screen-cast Lightbeam.

#### Explore Trackography & Lightbeam (15 min)
1. Divide participants between Trackography and Lightbeam, breaking into smaller groups if necessary.
2. Participants should explore the two tools, and discuss their findings.

#### Feedback and discussion (15 min)
Groups report back on what was discovered. Fill in gaps and offer explanations as needed. The following concepts should be covered:
1. What is tracking?
2. What type of data is being collected, and by whom?
3. What is Profiling?
4. How can companies track me across website? What is browser fingerprinting? (Demo EFF's Panopticlick.)
5. Trace-routes: how data travels across the internet.
6. Go over how the internet works, if needed.


-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
