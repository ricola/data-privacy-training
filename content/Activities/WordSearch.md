# Word Search
--- meta
title: "Word Search: Find out which different services Google and Facebook run"
uuid: d8ff3a89-8879-4332-a254-2525604badbb
lan: en
source: Tactical Tech
item: Activity
tags:
  - Social media
  - Choosing tools
duration: 15
description: Find as many services as you can that are owned by a particular company.
---

## Meta information

### Description
Find as many services as you can that are owned by a particular company (eg Google or Facebook).


### Duration
20 minutes.


### Ideal Number of Participants
Can be done with any number of participants.


### Learning Objectives
##### Knowledge
- Expand knowledge about services owned by a particular company.

### Materials and Equipment Needed
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)
- @[material](9793b1a6-5b29-4831-b7b7-05fcc5d31891)
- @[material](f2afe09c-14df-4e54-9e45-ed8062e4ef78)

## Activity: "Word Search"
--- meta
uuid: 20b6a14f-cb4b-4856-941f-c9af273636f1
---

#### Preparation
- Print out one word search for each participant. (If no word search exists for the service you want to talk about, create your own!)


#### Fill in the Word Search
1. Give one Word Search to each participant. Put them in pairs and give them 10-15 minutes to find as many services as they can.
2. Afterwards, ask them to highlight or colour in all of the services that they use.

#### Feedback and discussion
1. Bring the group back together and ask them to report back on all of the services that they found. Fill in any services which were missed.
2. Either follow up with a brief discussion about how these are all interconnected, and how your data might flow between them, or go on to the activity "Word Search: Data Traces."

-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
