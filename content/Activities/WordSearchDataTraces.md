# Word Search: Data Traces
--- meta
title: Word Search - Data Traces
uuid: 2912c11b-1327-4c65-972a-5d01a2dfe0c1
lan: en
source: Tactical Tech
item: Activity
tags:
  - Social media
  - Choosing tools
duration: 30
description: This activity follows on from the Word Search Activity. What insights might a company get into your life, habits and social networks?
---

## Meta information

### Description
This activity follows on from the Activity called "Word Search". What insights might a company get into your life, habits and social networks?

### Duration
30 minutes.

### Ideal Number of Participants
Can be done with any number of participants.

### Learning Objectives
##### Knowledge
- Gain insight into how many, and what types, of data traces you leave when you use the various products and services of a big company like Google or Facebook.
- See what all these data traces taken together might look like, in terms of a detailed profile.


### Materials and Equipment Needed
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)
- @[material](9793b1a6-5b29-4831-b7b7-05fcc5d31891)
- @[material](f2afe09c-14df-4e54-9e45-ed8062e4ef78)

## Activity: Word Search - "Data Traces"
--- meta
uuid: afa7b0e5-e772-4523-a01b-381fb5d16e49
---

#### Preparation
1. _Make sure participants have done the "Word Search" activity before doing this one!_
2. Map the data traces related to the different products/services found in the Wordsearch.

#### Map out the data traces (10 min)
1. Break the participants into small groups of 3-4. For each of the services they found in the Word Search, they should write down:
    - What is this product/service used for? (e.g. communication, news, posting photos/videos, connectivity etc)
    - What data traces are left through using this product/service?

#### Feedback and discussion (15 min)
Bring the group back together. Have a discussion that covers some of the following:
1. What sort of profile can this company create, if they connect up all these traces?
2. What does it mean to use many services owned by the same company? Talk about centralization -  why might we not want to put all our eggs in one basket?
3. Compartmentalization: What does it mean to compartmentalise your data traces? Why might this be a good thing to do? (access, usage, etc)
4. Ways to think about what we share: What information can be public, what information do we want to share with trusted individuals, and what information do we want to keep to ourselves?
5. Profiling, discrimination --> (use case studies if you have them)
6. Trust: Business models and privacy policies 

#### Wrap up (10 min)
1. If the following point has not yet become clear, sum up: Companies like Facebook and Google (or the company you have focused on in this exercise, if not Google or Facebook) own a lot of different products/services which serve a lot of different needs (photo sharing, communication, news etc). This, however, means that the company can piece together a fairly detailed overall picture of you.
2. Answer questions and fill in gaps as needed, and direct participants to resources


-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
