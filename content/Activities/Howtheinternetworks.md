# How the Internet works
--- meta
title: How the internet works
uuid: e0d34b63-d015-4338-9081-7ac67f26da92
lan: en
source: Tactical Tech
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Drawing exercise
duration: 30
description: See how the internet works, how you can connect to the internet in different ways, and what third parties can see along the way.
---

## Meta information

### Description
See how the internet works, how you can connect to the internet in different ways, and what third parties can see along the way.


### Duration
30 - 60 minutes.


### Ideal Number of Participants
This session can be done with any number of participants.


### Learning Objectives
##### Knowledge
- Understand the infrastructure of the internet, and who owns which parts of this infrastructure
- See where vulnerabilities lie: who can see what along the way
- Understand basic concepts: http and https, VPN and Tor

##### Attitude
- The internet is a physical thing and there are choices you can make about how securely your data travels through it.

### References
- [EFF diagram of the internet](https://www.eff.org/pages/tor-and-https): When you use the internet, who can see what information and at which points along the way? Includes different scenarios: VPN, Tor, Proxy, and HTTPS.


### Materials and Equipment Needed
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
- @[material](c0358b51-fe16-47ae-9686-927ec39d18f6)
- @[material](16c01d17-9ba7-47d6-815a-75cf9633004f)
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)
- @[material](1d01a081-f4a9-408b-b06b-78ee6288d1ce)


## Activity: "How the Internet Works"
--- meta
uuid: e72495d3-4b5f-49c9-a19d-10e067a97add
---
#### Preparation
1. Choose whether you are going to explain 'How the Internet Works' using (1) Cards, or (2) paper and pen.
2. Be prepared to draw the internet, including how it works differently when using a HTTP/HTTPS, a VPN, a proxy, or Tor. Have a diagram at hand as a guide.
3. *Cards*: If you're running the Cards version of this activity, print out ready-made Internet Infrastructure cards from https://myshadow.org/materials, or create your own. If the group if big, you may need to print out multiple sets. Ideally there should be one card per person.

#### Option 1 - CARDS
#### Visualise the internet
1. Depending on numbers, this exercise can be done all together or in  groups. Give each group one set of cards and ask them to put the cards in the correct order, showing how an email is sent from one person to another.
2. Compare results and go through the order together. Ask participants if there are specific concepts that are unclear, and clarify.

##### HTTP & HTTPS
Ask participants to explain the difference between how a message travels with HTTP and with HTTPS, giving them the envelope cards to demonstrate.

##### VPN, Proxy, and Tor
Ask participants to move to represent how a computer connects to a website using a VPN, a Proxy, or Tor. Ask them to show where vulnerabilities lie: Who can see what, at which points along the network?

#### Option 2 - PAPER AND PEN
##### Draw the internet
1. This exercise can be done individually or in groups of two or three. Hand out paper and pens to each participant or group, and write up some key words on the flip chart: server, router, national gateway. Ask participants to draw what the internet looks like, showing how an email travels from one computer to another.
2. Have the groups present their drawings and discuss differences and similarities. Then draw the internet on a flipchart.

##### HTTP & HTTPS
Ask participants to explain the difference between how a message travels with HTTP and with HTTPS. Explain or discuss.

##### VPN, Proxy, Tor
Explain or discuss the differences between a VPN, a Proxy or Tor. Also show where the vulnerabilities lie - who can see what, where.

### Key concepts to cover
1. The infrastructure of the internet
2. The difference between http and https
3. VPNs and Tor

-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
