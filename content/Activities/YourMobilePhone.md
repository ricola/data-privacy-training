# Your mobile phone: a breakdown in four layers
--- meta
title: Your mobile phone - A breakdown in 4 layers
uuid: 969fbfdb-3a2a-4459-9533-2e05c00c0198
lan: en
source: Tactical Tech
item: Activity
tags:
  - Mobile
duration: 40
description: Break down a mobile phone into parts, and see which third parties have access to what.
---

## Meta information

### Description
Break down a mobile phone into parts, and see which third parties have access to what.


### Duration
30-45 minutes.


### Ideal Number of Participants
This activity can be done with any number of participants.


### Learning Objectives

##### Knowledge
- Understand the basics of how a mobile phone works, in terms of both hardware and software.
- Understand how different parts of the mobile phone are used for information collection and tracking.

##### Attitude
- Mobile phones are generally insecure devices.
- Protecting the privacy of yourself and others involves making choices about how you use your phone.

### References
- Download the _Mobiles Reference Document_ from the link below, or find it [here](https://myshadow.org)
- [Location Tracking](https://myshadow.org/location-tracking)(MyShadow / Tactical Tech)

### Materials and Equipment Needed
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](a1108a3a-7bd8-4afc-9255-02a181ccffef)
- @[material](412c1eb9-9fc6-4f6b-975a-6d7c1915f750)

### Optional Materials and Handouts
- @[material](9824a3dd-12ed-4684-9afa-dd75118404ee)



## Activity: "Your Mobile Phone - A breakdown in 4 Layers"
--- meta
uuid: be34b4fa-dbc9-4b15-aa3e-b9354ddf5ee8
---

#### Preparation
1. Make sure you have the  _Mobiles Reference Document_.
2. Download the pdf _What is a Mobile Phone?_ and either print out one copy per 4-6 participants, or project the pdf onto a screen.
(If you have not downloaded these from the Materials Needed section, you can find them [here](https://myshadow.org/materials).


### Intro: What's a mobile phone?
From the group, elicit components of a mobile phone and write them up on a flipchart. Possible answers could include:
- keyboard
- screen
- antennae
- SIM card
- microphone
- speaker
- microphone
- battery
- baseband

- If participants hesitate, ask targeted questions like
  - How does the phone record your voice? (It has a microphone)
  - How does the phone store your contacts? (It has memory, like a PC hard drive).

##### A mobile phone in 4 layers
Walk the group through each page of the pdf and clarify any terms or aspects of the mobile phone that are unclear. A detailed breakdown can be found in the _Mobiles Reference Document_.

##### Focus on one layer
1. Set the following questions for group discussion (each group only needs to answer the questions relevant to their own page):
    - What data is created? (Only pages "Core", "Smart", "Operating System")
    - What are the risk involved with sharing this data? (Only page "Data Traces")
    - Who has access to this data? (All pages)
    - What are different actions you can take to increase your privacy and security? (All pages)

2. Bring the group together for feedback, clarifying anything that's unclear.

##### Data traces
Get particpants back into the same groups and set the following questions:
1. For the groups with *Core, Smart, and Operating System*: Focusing on the items on your page, what can you do to increase your privacy and get more control over your mobile phone?
2. For the group with *Data Traces*: What are the risks involved in creating and sharing these data traces? (Also think about these traces being connected together)

##### Feedback
Each group presents their findings. After each presentation, ask the other groups if there is anything they can add, and offer clarification/information where needed.


-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
