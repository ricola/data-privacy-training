# Mobile Phone Settings (Hands-on)
--- meta
title: Mobile Phone Settings (Hands-on)
uuid: 6bb57df8-8fbc-49eb-9a43-0069c4320a57
lan: en
source: Tactical Tech
item: SLE
tags:
  - Mobile
  - Hands-on
duration: 130
description: Prevent others from accessing your data, by changing some key settings.
---

## Meta information

### Description
Prevent others from accessing your data, by changing some key settings.

### Overview
1. Introductions (10 min)
2. Why is your phone insecure? (20 min)
3. Hands-On (60 min)
4.\\\[optional\\\] Strategies of Resistance (30 min)
5. Wrap up (10 min)

### Duration
130 minutes (excluding breaks)


### Ideal Number of Participants
This session splits up participants into two groups according to their operating systems, iOS or Android, so there should be a mimimum of two facilitators, ideally with a third facilitator to step in where needed.

- 2-20 participants: 2-3 trainers
- 20-28 participants: 4 trainers

### Learning Objectives

#### Knowledge
- Know which types of data are collected by default through mobile phones.

##### Skill
- Be able to increase the security of your mobile phone.
- Be able to configure a phone's settings to limit default data collection.

##### Attitude
- Mobile phones are by default largely insecure, but that there are some simple ways to increase control over your data.

### References
- [Android: basic setup](https://securityinabox.org/en/guide/basic-setup/android)  (Security in a Box / Tactical Tech)
- [How to encrypt your iphone](https://ssd.eff.org/en/module/how-encrypt-your-iphone) (Surveillance Self-Defence Guide / EFF)

### Materials and Equipment Needed
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](e96c589f-f1c5-49de-8493-ca39de05a502)
- @[material](412c1eb9-9fc6-4f6b-975a-6d7c1915f750)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
- @[material](884b8427-f8ba-441c-a663-b32282aaeb41)
- @[material](9824a3dd-12ed-4684-9afa-dd75118404ee)


## Steps
Note: this Hands-On Session requires the REFERENCE DOCUMENT FOR MOBILES.

### Step 1: Introductions (10 min)
Briefly introduce yourself and the session, then ask participants to introduce themselves and answer the following questions:
- What do you use your phone for?
- What do you want to learn in this session?

Taking expectations into account, give a brief overview of the session, including objectives, what will be covered (and what not), and how much time is available.


### Step 2. Why is your phone insecure? (20 min)
Walk the group through the following questions:

1. How are data traces created on your mobile phone?
    - The infrastructure of mobile communication means that certain types of data are created automatically - including precise location tracking data.
    - Many more data traces are created when we use the phone and the applications on it (e.g. maps, facebook, email, other apps).

2. What happens to these traces once they are created?
    - Many of these traces are stored on the phone itself, which could have implications if your phone gets into the hands of others.
    - However many data traces are also collected by companies and institutions without your active consent.

3. Do you have any control?  
    - The good news is that you do have _some_ control over what data is created and collected. Privacy and digital security are not all-or-nothing things: every small step matters. The important thing is to get started and make it work for you.


### Step 3. Hands-on (60 min)
Ask participants to take out their mobile phones and split them into two groups: Android & iPhone.

Go through the Hands-On Checklist (See the _Reference Document for Mobiles_), focusing specifically on:

1. **Check basic security and privacy**
    - Email
    - Phone "name"
    - Encryption
    - SIM card lock
    - Wi-Fi & Bluetooth
    - Passwords
    - Location settings
    - backup & reset

2. **Limit App Access & Permissions**
    - Check and limit app permissions,
    - Evaluate installed apps and delete unnecessary apps.
    - Opt out of profile-based advertising (Android)

3. **\\\[optional\\\] Change and customise your browser & change the way you access networks:** This section should cover the following:

    - **Compare, choose, and customise**
      - Go through an overview of the different browsers available for the OS you're focusing on, and then:
      - **Android:** Install and customise Firefox (only browser on Android where settings can be adjusted).
      - **iPhone:** Customise Safari (only browser on iPhone where settings can be adjusted).

    - **Install a VPN or Tor**
      - **Android**
        - Install Orfox and Orbot
        - Discuss alternative app stores and help participants configure their phone settings to accept apps from other sources if they want to use these alternatives.
      - **iPhone**:
        - Since Tor (via Orfox and Orbot) can not be used via an iPhone, a VPN is the next best option. Show participants how to install a VPN, and have them set one up if possible.


##### Tips: "How to run a Hands-On Session"
@[activity](ab6c7bf6-330c-428b-99c8-03aad4552ddf)

### Step 4 \\\[optional\\\] Strategies of Resistance (30 min)
Run a 'Strategies of Resistance' exercise, or an adapted, shorter variation of it, to test knowledge. See the *Mobiles Reference Guide* for mobile-specific examples.


##### Activity: "Strategies of Resistance"
@[activity](1667798e-fb3d-4ae6-a744-ddc6ae44bdf8)


### Step 5: Wrap up (10 min)
1. See if anything is unclear, and answer questions
2. Direct participants to resources
3. Hand out Tactical Tech's Pocket Privacy guide for Mobiles if you have them.



-------------------------------
<!---
data-privacy-training/Exhibition/TEMPLATE
-->
