**Notes from discussion**
add: cross referencing amongst session, try this session before or try this one after. Maybe add as tips
Where to include feedback of the facilitator on specific steps.

Need to create tip section:
old people multiply by two, young people a lot of activities.

# Title:
--- meta
title:
uuid:
lan:
source:
item:
tags:
duration:
description:
---

## Meta information

### Description

### Overview

### Duration


### Ideal Number of Participants

### Learning Objectives
##### Knowledge

##### Skills

##### Attitude


### References

### Materials and Equipment Needed

### Optional Materials and Handouts


## Steps

### Step 1:

### Step 2:

### Step 3:

### Step 4:

### Step 5:
